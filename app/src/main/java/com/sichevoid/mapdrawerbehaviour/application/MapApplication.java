package com.sichevoid.mapdrawerbehaviour.application;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.sichevoid.mapdrawerbehaviour.data.impl.dispatcher.DataDispatcher;

/**
 * Application class
 */
public class MapApplication extends Application {

    /**
     * (non-Javadoc)
     * @see Application#onCreate()
     */
    @Override
    public void onCreate() {
        super.onCreate();
        initFacebook();
        initService();
    }

    private void initFacebook() {
        FacebookSdk.sdkInitialize(this);
    }

    private void initService() {
        DataDispatcher.getInstance().createServices(new int[] {
                DataDispatcher.POI_SERVICE_ID_FACEBOOK
        });
    }
}
