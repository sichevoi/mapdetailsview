package com.sichevoid.mapdrawerbehaviour.behaviour;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.sichevoid.mapdrawerbehaviour.R;

/**
 * Created by elvirity on 07/02/16.
 */
public class ParallaxImageBehaviour extends Behavior<ImageView> {

    private final int dependencyId;

    private final int offsetBottom;
    private final int offsetTop;
    private final int displayHeight;

    /**
     * Constructor for xml creation.
     *
     * @param context Context to use
     * @param attrs Attributes
     */
    public ParallaxImageBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ParallaxImage_Params);
        dependencyId = a.getResourceId(R.styleable.ParallaxImage_Params_dependency_id, 0);

        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        displayHeight = metrics.heightPixels;

        this.offsetBottom = displayHeight - a.getDimensionPixelSize(R.styleable.ParallaxImage_Params_lower_height, 0);
        this.offsetTop = a.getDimensionPixelSize(R.styleable.ParallaxImage_Params_upper_top, 0);

        a.recycle();
    }

    /**
     * Constructor.
     *
     * @param dependencyId Dependency id
     * @param lowerHeight Dependency bottom position height
     * @param upperTop Dependency upper position top
     */
    public ParallaxImageBehaviour(Context context, int dependencyId, int lowerHeight, int upperTop) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        displayHeight = metrics.heightPixels;

        this.offsetBottom = displayHeight - lowerHeight;
        this.offsetTop = upperTop;

        this.dependencyId = dependencyId;
    }

    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, ImageView child, int layoutDirection) {
        final int left = 0;
        final int right = child.getMeasuredWidth();
        final int top = displayHeight;
        final int bottom = displayHeight + child.getMeasuredHeight();

        child.layout(left, top, right, bottom);

        return true;
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, ImageView child, View dependency) {
        return dependency.getId() == dependencyId;
    }

    /**
     * (non-Javadoc)
     * @see Behavior#onDependentViewChanged(CoordinatorLayout, View, View)
     */
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, ImageView child, View dependency) {
        ViewCompat.offsetTopAndBottom(child, calculateOffset(child.getTop(), dependency.getTop()));
        tickleInvalidationFlag(child);
        return false;
    }

    private static void tickleInvalidationFlag(View view) {
        final float x = ViewCompat.getTranslationX(view);
        ViewCompat.setTranslationX(view, x + 1);
        ViewCompat.setTranslationX(view, x);
    }

    private int calculateOffset(int childTop, int dependencyTop) {
        if (dependencyTop < offsetTop) {
            return 0;
        } else if (dependencyTop > offsetBottom) {
            return dependencyTop - childTop;
        } else {
            int targetTop = dependencyTop - (int) (((double) (offsetBottom - dependencyTop) / (offsetBottom - offsetTop)) * offsetTop);
            return targetTop - childTop;
        }
    }
}

