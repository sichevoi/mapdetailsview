package com.sichevoid.mapdrawerbehaviour.behaviour;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.sichevoid.mapdrawerbehaviour.R;

import java.lang.ref.WeakReference;

/**
 * Search toolbar behaviour. Hides when the drawer view moves up, shows when the drawer view hits bottom position.
 *
 * @author Elvira Sichevaia
 */
public class SlidingToolbarBehaviour extends CoordinatorLayout.Behavior<Toolbar> {

    /**
     * Show on dependency moving down
     */
    public static final int UP = 0;

    /**
     * Show on dependency moving up
     */
    public static final int DOWN = 1;

    /**
     * Dependency view id
     */
    private int dependencyId;
    protected int drawerThresholdTop;

    /**
     * States
     */
    public static final int STATE_SHOWN = 0;
    public static final int STATE_HIDDEN = 1;

    protected int state;

    /**
     * Child view reference
     */
    protected WeakReference<View> childRef;

    /**
     * View repositioning helper
     */
    protected ViewDragHelper viewDragHelper;
    protected SettleRunnable settleRunnable;

    /**
     * Behaviour direction
     */
    private final int direction;

    /**
     * Constructor for creating from xml.
     *
     * @param context Context to use
     * @param attrs Attributes
     */
    public SlidingToolbarBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.SlidingToolbar_Params);
        dependencyId = a.getResourceId(R.styleable.SlidingToolbar_Params_dependency_id, 0);
        drawerThresholdTop = a.getDimensionPixelSize(R.styleable.SlidingToolbar_Params_threshold_top, 0);
        if (drawerThresholdTop == 0) {
            DisplayMetrics metrics = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(metrics);
            int displayHeight = metrics.heightPixels;
            int thresholdHeight = a.getDimensionPixelSize(R.styleable.SlidingToolbar_Params_threshold_height, 0);
            drawerThresholdTop = displayHeight - thresholdHeight;
        }
        if (a.hasValue(R.styleable.SlidingToolbar_Params_direction)) {
            direction = a.getInt(R.styleable.SlidingToolbar_Params_direction, UP);
        } else {
            direction = UP;
        }

        if (a.getBoolean(R.styleable.SlidingToolbar_Params_shown, false)) {
            state = STATE_SHOWN;
        } else {
            state = STATE_HIDDEN;
        }

        a.recycle();
    }

    /**
     * Constructor.
     *
     * @param dependencyId Dependency id
     * @param shownInitially Whether the toolbar should be shown initially
     * @param drawerCollapsedTop Dependency view top in bottom position
     * @param direction {@link #UP} for showing the toolbar when the dependency moves up, {@link #DOWN} show when the dependency moved down
     */
    public SlidingToolbarBehaviour(int dependencyId, boolean shownInitially, int drawerCollapsedTop, int direction) {
        this.dependencyId = dependencyId;

        if (shownInitially) {
            state = STATE_SHOWN;
        } else {
            state = STATE_HIDDEN;
        }

        drawerThresholdTop = drawerCollapsedTop;
        this.direction = direction;
    }

    /**
     * (non-Javadoc)
     * @see Behavior#layoutDependsOn(CoordinatorLayout, View, View)
     */
    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, Toolbar child, View dependency) {
        return dependency.getId() == dependencyId;
    }

    /**
     * (non-Javadoc)
     * @see Behavior#onLayoutChild(CoordinatorLayout, View, int)
     */
    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, Toolbar child, int layoutDirection) {
        childRef = new WeakReference<View>(child);

        if (viewDragHelper == null) {
            viewDragHelper = ViewDragHelper.create(parent, dragCallback);
        }

        if (state == STATE_HIDDEN) {
            final int left = 0;
            final int right = child.getMeasuredWidth();
            final int top = -child.getMeasuredHeight();
            final int bottom = 0;

            child.layout(left, top, right, bottom);
        } else {
            final int left = 0;
            final int right = child.getMeasuredWidth();
            final int top = 0;
            final int bottom = child.getMeasuredHeight();

            child.layout(left, top, right, bottom);
        }

        return true;
    }

    /**
     * (non-Javadoc)
     * @see Behavior#onDependentViewChanged(CoordinatorLayout, View, View)
     */
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, Toolbar child, View dependency) {
        boolean positionChanged = false;
        int dependencyTop = dependency.getTop();
        switch (state) {
            case STATE_SHOWN:
                if (!isSettling(STATE_HIDDEN)) {
                    if (direction == DOWN && dependencyTop < drawerThresholdTop) {
                        animatePositionTo(child, -child.getHeight(), STATE_HIDDEN);
                        positionChanged = true;
                    } else if (direction == UP && dependencyTop > drawerThresholdTop) {
                        animatePositionTo(child, -child.getHeight(), STATE_HIDDEN);
                        positionChanged = true;
                    }
                }
                break;
            case STATE_HIDDEN:
                if (!isSettling(STATE_SHOWN)) {
                    if (direction == DOWN && dependency.getTop() >= drawerThresholdTop) {
                        animatePositionTo(child, 0, STATE_SHOWN);
                        positionChanged = true;
                    } else if (direction == UP && dependencyTop <= drawerThresholdTop) {
                        animatePositionTo(child, 0, STATE_SHOWN);
                        positionChanged = true;
                    }
                }
                break;
        }
        return positionChanged;
    }

    /**
     * Animate view position to requested top.
     *
     * @param child Child view
     * @param top Target view top position
     * @param targetState Target behaviour state
     */
    protected void animatePositionTo(final Toolbar child, int top, int targetState) {
        if (viewDragHelper.smoothSlideViewTo(child, child.getLeft(), top)) {
            settleRunnable = new SettleRunnable(child, targetState);
            ViewCompat.postOnAnimation(child, settleRunnable);
        } else {
            state = targetState;
        }
    }

    /**
     * Get whether view position is currently settling to the target state.
     *
     * @param targetState Target state
     * @return true if settling to the requested state
     */
    protected final boolean isSettling(int targetState) {
        return settleRunnable != null && settleRunnable.targetState == targetState;
    }

    /**
     * Tickle view invalidation flag to redraw after position update.
     *
     * @param view View
     */
    private static void tickleInvalidationFlag(View view) {
        final float x = ViewCompat.getTranslationX(view);
        ViewCompat.setTranslationX(view, x + 1);
        ViewCompat.setTranslationX(view, x);
    }

    /**
     * View drag callback
     */
    private ViewDragHelper.Callback dragCallback = new ViewDragHelper.Callback() {
        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return child == childRef.get();
        }
    };

    /**
     * View position settle runnable
     */
    private class SettleRunnable implements Runnable {
        private final View view;
        private final int targetState;

        SettleRunnable(View view, int targetState) {
            this.view = view;
            this.targetState = targetState;
        }

        @Override
        public void run() {
            if (viewDragHelper != null && viewDragHelper.continueSettling(true)) {
                tickleInvalidationFlag(view);
                ViewCompat.postOnAnimation(view, this);
            } else {
                state = targetState;
                settleRunnable = null;
            }
        }
    }
}
