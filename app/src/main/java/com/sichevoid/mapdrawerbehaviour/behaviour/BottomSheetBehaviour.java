package com.sichevoid.mapdrawerbehaviour.behaviour;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.sichevoid.mapdrawerbehaviour.R;

import java.lang.ref.WeakReference;

/**
 * Sliding bottom drawer behaviour.
 *
 * @author Elvira
 */
public class BottomSheetBehaviour extends Behavior<NestedScrollView> {
    private static final String LOG_TAG = BottomSheetBehaviour.class.getSimpleName();

    private int topHidden;
    private int topCollapsed;
    private int topHalfExpanded;
    private int topExpanded;
    private int topAnchorId;

    private State state = State.HIDDEN;

    private WeakReference<View> childRef;

    private enum State {
        HIDDEN {
            @Override
            protected int getBaseTop() {
                return parent.topHidden;
            }

            @Override
            protected int getMinTop() {
                return parent.topHidden;
            }

            @Override
            protected int getMaxTop() {
                return parent.topHidden;
            }
        },
        COLLAPSED {

            @Override
            protected void onTransitionEnd(View child) {
                super.onTransitionEnd(child);
                setHeaderVisibility(child, false);
            }

            @Override
            protected int getBaseTop() {
                return parent.topCollapsed;
            }

            @Override
            protected int getMinTop() {
                return parent.topHalfExpanded;
            }

            @Override
            protected int getMaxTop() {
                return parent.topCollapsed;
            }

            @Override
            public boolean onLayoutChild(CoordinatorLayout parent, NestedScrollView child, int layoutDirection) {
                setHeaderVisibility(child, false);
                return super.onLayoutChild(parent, child, layoutDirection);
            }

            @Override
            public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target, int dx, int dy, int[] consumed) {
                setHeaderVisibility(child, true);
                int offset = calculateNestedScrollOffset(child, dy);
                child.offsetTopAndBottom(offset);
                consumed[1] = dy;
            }

            @Override
            public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target) {
                if (child.getTop() > (getMaxTop() - (getMaxTop() - getMinTop()) / 4)) {
                    transitionTo(child);
                } else {
                    HALF_EXPANDED.transitionTo(child);
                }
            }

            private void setHeaderVisibility(View child, boolean visible) {
                if (parent.topAnchorId > 0) {
                    View header = child.findViewById(parent.topAnchorId);
                    if (header != null) {
                        header.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
                    }
                }
            }
        },
        HALF_EXPANDED {
            @Override
            public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target, int dx, int dy, int[] consumed) {
                int offset = calculateNestedScrollOffset(child, dy);
                child.offsetTopAndBottom(offset);
                // we don't allow scrolling of the scroll view content in this state, so consume everything
                consumed[1] = dy;
            }

            @Override
            public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target) {
                if (child.getTop() < (getBaseTop() - (getBaseTop() - getMinTop()) / 3)) {
                    EXPANDED.transitionTo(child);
                } else if (child.getTop() < (getBaseTop() + (getMaxTop() - getBaseTop()) / 3)) {
                    transitionTo(child);
                } else {
                    COLLAPSED.transitionTo(child);
                }
            }

            @Override
            protected int getBaseTop() {
                return parent.topHalfExpanded;
            }

            @Override
            protected int getMinTop() {
                return 0;
            }

            @Override
            protected int getMaxTop() {
                return parent.topCollapsed;
            }
        },
        EXPANDED {
            @Override
            public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target, int dx, int dy, int[] consumed) {
                if (ViewCompat.canScrollVertically(child, dy)) {
                    consumed[1] = 0;
                } else {
                    int offset = calculateNestedScrollOffset(child, dy);
                    child.offsetTopAndBottom(offset);

                    // consume all or none
                    if (offset == 0) {
                        consumed[1] = 0;
                    } else {
                        consumed[1] = dy;
                    }
                }
            }

            @Override
            public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target) {
                if (child.getTop() < (getBaseTop() + (HALF_EXPANDED.getBaseTop() - getBaseTop()) / 3)) {
                    transitionTo(child);
                } else if (child.getTop() < HALF_EXPANDED.getBaseTop()) {
                    HALF_EXPANDED.transitionTo(child);
                } else {
                    COLLAPSED.transitionTo(child);
                }
            }

            @Override
            protected int getBaseTop() {
                return anchorBottom;
            }

            @Override
            protected int getMinTop() {
                return anchorBottom;
            }

            @Override
            protected int getMaxTop() {
                return parent.topCollapsed;
            }
        };

        protected static int anchorBottom;
        protected static ViewDragHelper viewDragHelper;

        protected BottomSheetBehaviour parent;

        private int headerViewHeight;
        private int actionBarHeight;

        static void initAll(BottomSheetBehaviour parent) {
            for (State state : values()) {
                state.init(parent);
            }
        }

        protected void init(BottomSheetBehaviour parent) {
            this.parent = parent;
        }

        public boolean onStartNestedScroll(CoordinatorLayout parent, NestedScrollView child, View directTargetChild, View target, int nestedScrollAxes) {
            return  (nestedScrollAxes & ViewCompat.SCROLL_AXIS_VERTICAL) != 0;
        }

        public boolean onLayoutChild(CoordinatorLayout parent, NestedScrollView child, int layoutDirection) {
            final int left = 0;
            final int right = child.getMeasuredWidth();

            final int top;
            int bottom;

            top = getBaseTop();
            bottom = top + child.getMeasuredHeight();

            child.layout(left, top, right, bottom);

            if (this.parent.topAnchorId > 0) {
                headerViewHeight = child.findViewById(this.parent.topAnchorId).getHeight();
                actionBarHeight = child.getResources().getDimensionPixelSize(R.dimen.action_bar_size);
                int fullHeight = child.getMeasuredHeight() + headerViewHeight - actionBarHeight;
                bottom = top + fullHeight;

                anchorBottom = actionBarHeight - headerViewHeight;

                child.layout(left, top, right, bottom);
            }

            if (viewDragHelper == null) {
                viewDragHelper = ViewDragHelper.create(parent, dragCallback);
            }

            return true;
        }

        public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target, int dx, int dy, int[] consumed) {
        }

        public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target) {
        }

        /**
         * Transition to the state.
         *
         * @param child Child view
         */
        protected void transitionTo(NestedScrollView child) {
            animatePositionTo(child, getBaseTop(), this);
        }

        /**
         * On transition to the state ended.
         *
         * @param child Child view
         */
        protected void onTransitionEnd(View child) {
            parent.state = this;
        }

        /**
         * Compute nested scroll offset.
         *
         * @param child Child view
         * @param dy Scrolling distance
         * @return offset
         */
        protected int calculateNestedScrollOffset(NestedScrollView child, int dy) {
            int offset = 0;
            int childTop = child.getTop();

            boolean canOffsetUp = childTop <= getMaxTop() && dy > 0;
            boolean canOffsetDown = childTop >= getMinTop() && dy < 0;

            if (canOffsetUp || canOffsetDown) {
                if (childTop - dy > getMaxTop()) {
                    offset = getMaxTop() - childTop;
                } else if (childTop - dy < getMinTop()) {
                    offset = getMinTop() - childTop;
                } else {
                    offset = -dy;
                }
            }

            return offset;
        }

        /**
         * Animate view position to provided top position.
         *
         * @param child View to animate
         * @param top View target top position
         */
        protected void animatePositionTo(NestedScrollView child, final int top, State targetState) {
            if (viewDragHelper.smoothSlideViewTo(child, child.getLeft(), top)) {
                ViewCompat.postOnAnimation(child, new SettleRunnable(child, targetState));
            } else {
                targetState.onTransitionEnd(child);
            }
        }

        private ViewDragHelper.Callback dragCallback = new ViewDragHelper.Callback() {

            /**
             * (non-Javadoc)
             * @see android.support.v4.widget.ViewDragHelper.Callback#tryCaptureView(View, int)
             */
            @Override
            public boolean tryCaptureView(View child, int pointerId) {
                return child == parent.childRef.get();
            }
        };

        protected abstract int getBaseTop();
        protected abstract int getMinTop();
        protected abstract int getMaxTop();

        private class SettleRunnable implements Runnable {

            private final View view;

            private final State targetState;

            SettleRunnable(View view, State targetState) {
                this.view = view;
                this.targetState = targetState;
            }

            @Override
            public void run() {
                if (viewDragHelper != null && viewDragHelper.continueSettling(true)) {
                    ViewCompat.postOnAnimation(view, this);
                } else {
                    targetState.onTransitionEnd(view);
                }
            }
        }
    }

    /**
     * Default constructor for inflating BottomSheetBehaviors from layout.
     *
     * @param context The {@link Context}.
     * @param attrs   The {@link AttributeSet}.
     */
    public BottomSheetBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.BottomSheet_Params);
        int collapsedHeight = a.getDimensionPixelSize(R.styleable.BottomSheet_Params_collapsed_height, 0);
        topHalfExpanded = a.getDimensionPixelSize(R.styleable.BottomSheet_Params_half_expanded_top, 0);
        topAnchorId = a.getResourceId(R.styleable.BottomSheet_Params_top_anchor_id, 0);
        a.recycle();

        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        int displayHeight = metrics.heightPixels;

        topHidden = displayHeight;
        topCollapsed = displayHeight - collapsedHeight;
        topExpanded = 0;

        State.initAll(this);
    }

    /**
     * Constructor.
     *
     * @param displayHeight The display height
     * @param collapsedTop Collapsed state top position
     * @param halfExpandedTop Half-expanded state top position
     * @param expandedTop Expanded state top position
     */
    public BottomSheetBehaviour(int displayHeight, int collapsedTop, int halfExpandedTop, int expandedTop) {
        topHidden = displayHeight;
        topCollapsed = collapsedTop;
        topHalfExpanded = halfExpandedTop;
        topExpanded = expandedTop;

        State.initAll(this);
    }

    /**
     * (non-Javadoc)
     * @see android.support.design.widget.CoordinatorLayout.Behavior#onLayoutChild(CoordinatorLayout, View, int)
     */
    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, NestedScrollView child, int layoutDirection) {
        childRef = new WeakReference<View>(child);
        return state.onLayoutChild(parent, child, layoutDirection);
    }

    /**
     * (non-Javadoc)
     * @see Behavior#onStartNestedScroll(CoordinatorLayout, View, View, View, int)
     */
    @Override
    public boolean onStartNestedScroll(CoordinatorLayout parent, NestedScrollView child, View directTargetChild, View target, int nestedScrollAxes) {
        return state.onStartNestedScroll(parent, child, directTargetChild, target, nestedScrollAxes);
    }

    /**
     * (non-Javadoc)
     * @see Behavior#onNestedPreScroll(CoordinatorLayout, View, View, int, int, int[])
     */
    @Override
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target, int dx, int dy, int[] consumed) {
        state.onNestedPreScroll(coordinatorLayout, child, target, dx, dy, consumed);
    }

    /**
     * (non-Javadoc)
     * @see Behavior#onStopNestedScroll(CoordinatorLayout, View, View)
     */
    @Override
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, NestedScrollView child, View target) {
        state.onStopNestedScroll(coordinatorLayout, child, target);
    }

    public boolean isHidden() {
        return state == State.HIDDEN;
    }

    public void collapse(NestedScrollView child) {
        State.COLLAPSED.transitionTo(child);
        child.requestLayout();
    }

    public void hide(NestedScrollView child) {
        State.HIDDEN.transitionTo(child);
    }
}
