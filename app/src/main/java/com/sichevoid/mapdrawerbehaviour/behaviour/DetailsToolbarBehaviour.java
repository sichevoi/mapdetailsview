package com.sichevoid.mapdrawerbehaviour.behaviour;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import com.sichevoid.mapdrawerbehaviour.R;

import java.lang.ref.WeakReference;

/**
 * Details toolbar behaviour. In addition to sliding the toolbar in and out,
 * animates the child's background and title transparency.
 *
 * @author Elvira Sichevaia
 */
public class DetailsToolbarBehaviour extends SlidingToolbarBehaviour {

    private static final int ALPHA_ANIMATION_DURATION = 150;

    private int titleViewId;

    private ValueAnimator alphaAnimator;

    private int targetAlpha;

    private WeakReference<View> titleRef;

    /**
     * Constructor for creating from xml.
     *
     * @param context Context to use
     * @param attrs Attributes
     */
    public DetailsToolbarBehaviour(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.DetailsToolbar_Params);
        titleViewId = a.getResourceId(R.styleable.DetailsToolbar_Params_title_id, 0);

        a.recycle();
    }

    /**
     * Constructor.
     *
     * @param dependencyId       Dependency id
     * @param shownInitially     Whether the toolbar should be shown initially
     * @param drawerCollapsedTop Dependency view top in bottom position
     */
    public DetailsToolbarBehaviour(int dependencyId, boolean shownInitially, int drawerCollapsedTop, int titleViewId) {
        super(dependencyId, shownInitially, drawerCollapsedTop, UP);
        this.titleViewId = titleViewId;
    }

    /**
     * (non-Javadoc)
     * @see android.support.design.widget.CoordinatorLayout.Behavior#onLayoutChild(CoordinatorLayout, View, int)
     */
    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, Toolbar child, int layoutDirection) {
        titleRef = new WeakReference<>(child.findViewById(titleViewId));
        return super.onLayoutChild(parent, child, layoutDirection);
    }

    /**
     * (non-Javadoc)
     * @see android.support.design.widget.CoordinatorLayout.Behavior#onDependentViewChanged(CoordinatorLayout, View, View)
     */
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, Toolbar child, View dependency) {
        boolean updated = super.onDependentViewChanged(parent, child, dependency);

        int dependencyTop = dependency.getTop();

        if (state == STATE_SHOWN) {
            if (dependencyTop < child.getHeight()) {
                animateAlphaTo(child.getBackground(), 255);
            } else if (dependencyTop > child.getHeight()) {
                animateAlphaTo(child.getBackground(), 0);
            }
        }

        return updated;
    }

    /**
     * Animate background drawable's alpha. Does nothing if already animating or alpha is the same.
     *
     * @param drawable Drawable
     * @param targetAlpha Target alpha
     */
    private void animateAlphaTo(final Drawable drawable, final int targetAlpha) {
        if (targetAlpha == this.targetAlpha) {
            return;
        }

        this.targetAlpha = targetAlpha;

        if (alphaAnimator != null && alphaAnimator.isStarted()) {
            alphaAnimator.cancel();
        }

        if (alphaAnimator == null) {
            alphaAnimator = ValueAnimator.ofInt();
            alphaAnimator.setInterpolator(new AccelerateInterpolator());
            alphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    int alpha = (Integer) animation.getAnimatedValue();

                    drawable.setAlpha(alpha);

                    View titleView = titleRef.get();
                    if (titleView != null) {
                        titleView.setAlpha(alpha);
                    }
                }
            });
            alphaAnimator.setDuration(ALPHA_ANIMATION_DURATION);
        }

        alphaAnimator.setIntValues(targetAlpha == 0 ? 255 : 0, targetAlpha == 0 ? 0 : 255);
        alphaAnimator.start();
    }
}
