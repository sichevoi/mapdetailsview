package com.sichevoid.mapdrawerbehaviour.behaviour;

import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.View;

/**
 * Created by elvirity on 23/02/16.
 */
public class ViewPositionHelper {

    private View view;

    public ViewPositionHelper(View view) {
        this.view = view;
    }

    public void setTop(int top) {
        int currentTop = view.getTop();
        ViewCompat.offsetTopAndBottom(view, top - currentTop);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            tickleInvalidationFlag(view);
        }
    }

    private static void tickleInvalidationFlag(View view) {
        final float x = ViewCompat.getTranslationX(view);
        ViewCompat.setTranslationX(view, x + 1);
        ViewCompat.setTranslationX(view, x);
    }
}
