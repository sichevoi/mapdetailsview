package com.sichevoid.mapdrawerbehaviour.data.base.service;

import android.location.Location;

import com.sichevoid.mapdrawerbehaviour.data.base.entity.Poi;

import java.util.List;

/**
 * Point of interest data service.
 *
 * @author Elvira
 */
public interface PoiService {

    /**
     * Poi search request.
     *
     * @param request Request parameters
     * @return list of Poi
     */
    PoiResponse<List<Poi>> search(SearchRequest request);

    /**
     * Search request
     */
    class SearchRequest extends PageableRequest {
        /**
         * Search query, can be null
         */
        public String query;

        /**
         * Search center
         */
        public Location center;

        /**
         * Distance
         */
        public int distance;

        public SearchRequest query(String query) {
            this.query = query;
            return this;
        }

        public SearchRequest center(Location center) {
            this.center = center;
            return this;
        }

        public SearchRequest offset(int offset) {
            this.offset = offset;
            return this;
        }

        public SearchRequest limit(int limit) {
            this.limit = limit;
            return this;
        }
    }

    /**
     * Pageable request
     */
    class PageableRequest {
        /**
         * Page offset
         */
        public int offset;

        /**
         * Page limit
         */
        public int limit;
    }
}
