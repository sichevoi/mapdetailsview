package com.sichevoid.mapdrawerbehaviour.data.impl.facebook;

import android.support.annotation.NonNull;

import com.sichevoid.mapdrawerbehaviour.data.base.entity.PoiImage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by elvirity on 19/02/16.
 */
public class FacebookImage implements PoiImage {
    private static final String HEIGHT = "height";
    private static final String WIDTH = "width";
    private static final String SOURCE = "source";

    /**
     * The underlying JSON
     */
    private JSONObject jsonObject;

    /**
     * Constructor.
     *
     * @param jsonObject JSON
     */
    public FacebookImage(@NonNull JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public int getHeight() {
        try {
            return jsonObject.getInt(HEIGHT);
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int getWidth() {
        try {
            return jsonObject.getInt(WIDTH);
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public String getPath() {
        try {
            return jsonObject.getString(SOURCE);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
