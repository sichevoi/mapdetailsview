package com.sichevoid.mapdrawerbehaviour.data.impl.facebook;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Facebook page
 */
public class FacebookPage {
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String ABOUT = "about";
    private static final String CHECKINS_COUNT = "checkins";
    private static final String LIKES = "likes";
    private static final String PHOTOS = "photos";
    private static final String LOCATION = "location";
    private static final String CATEGORY = "category";
    private static final String DESCRIPTION = "description";

    private JSONObject jsonObject;

    public FacebookPage(@NonNull JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    String getPageId() {
        return safeGetString(ID);
    }

    String getPageName() {
        return safeGetString(NAME);
    }

    String getPageCategory() {
        return safeGetString(CATEGORY);
    }

    String getAbout() {
        return safeGetString(ABOUT);
    }

    String getPageDescription() {
        return safeGetString(DESCRIPTION);
    }

    long getCheckinsCount() {
        try {
            return jsonObject.getLong(CHECKINS_COUNT);
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

    long getLikesCount() {
        try {
            return jsonObject.getLong(LIKES);
        } catch (JSONException e) {
            e.printStackTrace();
            return -1;
        }
    }

    JSONObject getPhotos() {
        try {
            if (jsonObject.has(PHOTOS)) {
                return jsonObject.getJSONObject(PHOTOS);
            } else {
                return null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    JSONObject getPageLocation() {
        try {
            return jsonObject.getJSONObject(LOCATION);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String safeGetString(String name) {
        try {
            return jsonObject.getString(name);
        } catch (JSONException e) {
            return null;
        }
    }
}
