package com.sichevoid.mapdrawerbehaviour.data.base.service;

import com.sichevoid.mapdrawerbehaviour.data.base.entity.Poi;

import java.util.LinkedList;
import java.util.List;

/**
 * Poi service provider.
 *
 * @author Elvira
 */
public class PoiServiceProvider implements PoiService {
    /**
     * Provider instance
     */
    private static PoiServiceProvider instance = new PoiServiceProvider();

    /**
     * Services
     */
    private List<PoiService> services = new LinkedList<>();

    public void addService(PoiService service) {
        services.add(service);
    }

    public static PoiServiceProvider getInstance() {
        return instance;
    }

    @Override
    public PoiResponse<List<Poi>> search(SearchRequest request) {
        for (PoiService service : services) {
            PoiResponse<List<Poi>> res = service.search(request);
            if (res != null) {
                return res;
            }
        }
        return null;
    }
}
