package com.sichevoid.mapdrawerbehaviour.data.impl.facebook;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.sichevoid.mapdrawerbehaviour.data.base.entity.Poi;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiError;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiResponse;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Facebook Poi service.
 *
 * @author Elvira
 */
public class FacebookPoiService implements PoiService {

    public static class GraphError implements PoiError {
        private int errorCode;
        private GraphResponse graphResponse;

        public GraphError(int errorCode, GraphResponse graphResponse) {
            this.errorCode = errorCode;
            this.graphResponse = graphResponse;
        }

        @Override
        public int getCode() {
            return errorCode;
        }

        @Override
        public void resolve(ResolveCallback callback) {
            callback.resolve(graphResponse);
        }
    }

    private static final String LOG_TAG = FacebookPoiService.class.getSimpleName();

    /**
     * Graph paths
     */
    private static final String GRAPH_SEARCH_PATH = "/search";

    /**
     * Parameters
     */
    private static final String QUERY = "q";
    private static final String OFFSET = "offset";
    private static final String LIMIT = "limit";
    private static final String CENTER = "center";
    private static final String CENTER_VALUE_FORMAT = "%f,%f";
    private static final String DISTANCE = "distance";
    private static final String TYPE = "type";
    private static final String TYPE_VALUE_PLACE = "place";
    private static final String FIELDS = "fields";

    private static final String RESPONSE_FIELD_DATA = "data";

    private final int errorCode;

    public FacebookPoiService(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Get whether this Facebook service is available.
     *
     * @return true if available
     */
    public static boolean isAvailable() {
        return FacebookSdk.isInitialized();
    }

    /**
     * (non-Javadoc)
     * @see PoiService#search(SearchRequest)
     */
    @Override
    public PoiResponse<List<Poi>> search(final SearchRequest request) {

        Bundle params = new Bundle();

        if (request.query != null) {
            params.putString(QUERY, request.query);
        }

        if (request.center != null) {
            params.putString(CENTER, String.format(Locale.US, CENTER_VALUE_FORMAT, request.center.getLatitude(), request.center.getLongitude()));
        }

        if (request.distance != 0) {
            params.putInt(DISTANCE, request.distance);
        }

        if (request.limit != 0) {
            params.putInt(LIMIT, request.limit);
        }

        if (request.offset != 0) {
            params.putInt(OFFSET, request.offset);
        }

        params.putString(TYPE, TYPE_VALUE_PLACE);
        params.putString(FIELDS, "photos{images},name,about,checkins,likes,location,category,cover,description");

        GraphResponse response = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                GRAPH_SEARCH_PATH,
                params,
                HttpMethod.GET,
                null)
        .executeAndWait();

        PoiResponse<List<Poi>> res;

        if (response.getError() != null) {
            res = new PoiResponse<>(new GraphError(errorCode, response));
        } else {
            JSONObject graphObject = response.getJSONObject();
            List<Poi> poiList = new ArrayList<>();
            try {
                JSONArray data = graphObject.getJSONArray(RESPONSE_FIELD_DATA);
                for (int i = 0; i < data.length(); ++i) {
                    JSONObject object = data.getJSONObject(i);
                    FacebookPoi poi = new FacebookPoi(object);
                    poiList.add(poi);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            res = new PoiResponse<>(poiList);
        }

        return res;
    }
}
