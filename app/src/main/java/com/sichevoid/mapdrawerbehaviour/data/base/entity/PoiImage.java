package com.sichevoid.mapdrawerbehaviour.data.base.entity;

/**
 * Created by elvirity on 19/02/16.
 */
public interface PoiImage {
    int getHeight();
    int getWidth();
    String getPath();
}
