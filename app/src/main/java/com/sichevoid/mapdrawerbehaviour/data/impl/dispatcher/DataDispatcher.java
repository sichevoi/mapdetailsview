package com.sichevoid.mapdrawerbehaviour.data.impl.dispatcher;

import android.app.Activity;
import android.util.Log;

import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiError;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiServiceProvider;
import com.sichevoid.mapdrawerbehaviour.data.impl.facebook.FacebookPoiService;

/**
 * Data dispatcher. Mediates between the base service and implementations.
 */
public class DataDispatcher {
    public static final int POI_SERVICE_ID_FACEBOOK = 0;
    public static final int POI_SERVICE_ID_IZI_TRAVEL = 1;

    private static final int NUM_SERVICES = 2;

    private static final DataDispatcher instance = new DataDispatcher();

    private boolean created;

    private int[] serviceErrorCodes = new int[NUM_SERVICES];

    public static DataDispatcher getInstance() {
        return instance;
    }

    public void createServices(int[] serviceIds) {

        if (created == true) {
            throw new RuntimeException();
        }

        PoiServiceProvider serviceProvider = PoiServiceProvider.getInstance();
        for (int serviceId : serviceIds) {
            switch (serviceId) {
                case POI_SERVICE_ID_FACEBOOK:
                    int errorCode = PoiError.Registry.getNextErrorCode();
                    serviceErrorCodes[POI_SERVICE_ID_FACEBOOK] = errorCode;
                    serviceProvider.addService(new FacebookPoiService(errorCode));
                    break;
            }
        }
        created = true;
    }

    public PoiError.ResolveCallback getResolveCallback(PoiError error, Activity activity) {
        int serviceId = -1;
        for (int i = 0; i < serviceErrorCodes.length; ++i) {
            if (error.getCode() == serviceErrorCodes[i]) {
                serviceId = i;
                break;
            }
        }

        if (serviceId >= 0) {
            switch (serviceId) {
                case POI_SERVICE_ID_FACEBOOK:
                    return new GraphErrorResolveCallback(activity);
            }
        }

        return null;
    }

    private static class GraphErrorResolveCallback implements PoiError.ResolveCallback {
        private static final String LOG_TAG = GraphErrorResolveCallback.class.getSimpleName();

        private Activity activity;

        public GraphErrorResolveCallback(Activity activity) {
            this.activity = activity;
        }

        @Override
        public void resolve(Object error) {
            if (error instanceof GraphResponse) {
                LoginManager.getInstance().resolveError(activity, (GraphResponse) error);
            } else {
                Log.w(LOG_TAG, "Could not resolve unknown error " + error);
            }
        }
    }
}
