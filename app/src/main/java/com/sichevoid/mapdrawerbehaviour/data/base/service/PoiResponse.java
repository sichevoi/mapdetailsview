package com.sichevoid.mapdrawerbehaviour.data.base.service;

/**
 * Created by elvirity on 17/02/16.
 */
public class PoiResponse<T> {
    private T data;
    private PoiError error;

    public PoiResponse(T data) {
        this.data = data;
    }

    public PoiResponse(PoiError error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public PoiError getError() {
        return error;
    }
}
