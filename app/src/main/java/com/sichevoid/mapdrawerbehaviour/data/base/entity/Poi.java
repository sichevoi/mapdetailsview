package com.sichevoid.mapdrawerbehaviour.data.base.entity;

import android.location.Location;
import android.support.annotation.NonNull;

import java.util.List;

/**
 * Point of interest interface.
 *
 * @author Elvira
 */
public interface Poi {
    String getId();
    String getTitle();
    String getCategory();
    String getSummary();
    String getDescription();
    Location getLocation();
    @NonNull List<PoiImage> getImages();
    long getLikesCount();
    long getVisitsCount();
}
