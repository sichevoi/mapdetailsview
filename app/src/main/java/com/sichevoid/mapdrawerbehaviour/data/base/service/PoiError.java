package com.sichevoid.mapdrawerbehaviour.data.base.service;

/**
 * Created by elvirity on 17/02/16.
 */
public interface PoiError {

    interface ResolveCallback {
        void resolve(Object error);
    }

    int getCode();

    void resolve(ResolveCallback callback);

    class Registry {
        private static int lastRegisteredCode;
        public static synchronized int getNextErrorCode() {
            return lastRegisteredCode++;
        }
    }
}
