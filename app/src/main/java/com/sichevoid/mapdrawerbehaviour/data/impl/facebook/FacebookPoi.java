package com.sichevoid.mapdrawerbehaviour.data.impl.facebook;

import android.location.Location;
import android.support.annotation.NonNull;

import com.sichevoid.mapdrawerbehaviour.data.base.entity.Poi;
import com.sichevoid.mapdrawerbehaviour.data.base.entity.PoiImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elvirity on 19/02/16.
 */
public class FacebookPoi extends FacebookPage implements Poi {

    public FacebookPoi(@NonNull JSONObject jsonObject) {
        super(jsonObject);
    }

    @Override
    public String getId() {
        return super.getPageId();
    }

    @Override
    public String getTitle() {
        return getPageName();
    }

    @Override
    public String getCategory() {
        return getPageCategory();
    }

    @Override
    public String getSummary() {
        return getAbout();
    }

    @Override
    public String getDescription() {
        return getPageDescription();
    }

    @Override
    public Location getLocation() {
        JSONObject jsonLocation = getPageLocation();
        if (jsonLocation != null) {
            try {
                Location location = new Location("");
                location.setLongitude(jsonLocation.getDouble("longitude"));
                location.setLatitude(jsonLocation.getDouble("latitude"));
                return location;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public List<PoiImage> getImages() {
        List<PoiImage> facebookImages = new ArrayList<>();
        JSONObject photos = getPhotos();
        if (photos != null) {
            try {
                JSONArray images = photos.getJSONArray("data").getJSONObject(0).getJSONArray("images");
                for (int i = 0; i < images.length(); ++i) {
                    JSONObject imageObject = images.getJSONObject(i);
                    facebookImages.add(new FacebookImage(imageObject));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return facebookImages;
    }

    @Override
    public long getVisitsCount() {
        return getCheckinsCount();
    }

    @Override
    public long getLikesCount() {
        return super.getLikesCount();
    }
}
