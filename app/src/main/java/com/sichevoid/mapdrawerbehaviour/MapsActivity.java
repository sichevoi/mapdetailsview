package com.sichevoid.mapdrawerbehaviour;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sichevoid.mapdrawerbehaviour.behaviour.BottomSheetBehaviour;
import com.sichevoid.mapdrawerbehaviour.data.base.entity.Poi;
import com.sichevoid.mapdrawerbehaviour.data.base.entity.PoiImage;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiError;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiResponse;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiService;
import com.sichevoid.mapdrawerbehaviour.data.base.service.PoiServiceProvider;
import com.sichevoid.mapdrawerbehaviour.data.impl.dispatcher.DataDispatcher;
import com.sichevoid.mapdrawerbehaviour.databinding.ObjectDetailsBinding;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MapsActivity extends RxAppCompatActivity implements OnMapReadyCallback {
    private static final String LOG_TAG = MapsActivity.class.getSimpleName();

    private static float DEFAULT_MARKER_HUE = BitmapDescriptorFactory.HUE_BLUE;
    private static float SELECTED_MARKER_HUE = BitmapDescriptorFactory.HUE_MAGENTA;

    private GoogleMap map;
    private NestedScrollView detailsView;
    private ProgressBar searchPlaceProgress;

    private TextView detailsTitle;

    private ObjectDetailsBinding detailsBinding;
    private BottomSheetBehaviour drawerBehaviour;

    private CallbackManager facebookCallbackManager;

    private Map<String, Poi> objectsMap = new HashMap<>();
    private ImageView imageView;

    private MapLoadedCallback mapLoadedCallback;
    private MarkerCLickListener markerCLickListener;
    private CameraChangeListener cameraChangeListener;

    private boolean isDataLoading;

    /**
     * (non-Javadoc)
     * @see android.app.Activity#onCreate(Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        facebookCallbackManager = CallbackManager.Factory.create();

        loadData(getDefaultLocation());

        Toolbar detailsToolbar = (Toolbar) findViewById(R.id.details_toolbar);
        detailsToolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
        detailsToolbar.getBackground().setAlpha(0);
        detailsTitle = (TextView) detailsToolbar.findViewById(R.id.details_title);
        detailsTitle.setAlpha(0);
        setSupportActionBar(detailsToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        detailsView = (NestedScrollView) findViewById(R.id.details_drawer);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) detailsView.getLayoutParams();
        drawerBehaviour = (BottomSheetBehaviour) layoutParams.getBehavior();

        imageView = (ImageView) findViewById(R.id.image);

        View detailsContainer = detailsView.findViewById(R.id.details_container);
        detailsBinding = DataBindingUtil.bind(detailsContainer);

        Toolbar searchToolbar = (Toolbar) findViewById(R.id.search_toolbar);
        final EditText searchPlace = (EditText) searchToolbar.findViewById(R.id.search_place);
        searchPlace.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchPlace.getApplicationWindowToken(), 0);
                    searchPlace.clearFocus();
                    makeGeoSearch(v.getText().toString());
                    return true;
                } else {
                    return false;
                }
            }
        });

        searchPlaceProgress = (ProgressBar) findViewById(R.id.search_place_progress);
        searchPlaceProgress.setVisibility(View.GONE);
    }

    /**
     * (non-Javadoc)
     * @see android.app.Activity#onActivityResult(int, int, Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!facebookCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * (non-Javadoc)
     * @see OnMapReadyCallback#onMapReady(GoogleMap)
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(LOG_TAG, "Map is ready");

        map = googleMap;

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (!drawerBehaviour.isHidden()) {
                    drawerBehaviour.hide(detailsView);
                }
            }
        });

        googleMap.setOnMarkerClickListener(markerCLickListener = new MarkerCLickListener());
        googleMap.setOnCameraChangeListener(cameraChangeListener = new CameraChangeListener());

        if (!objectsMap.isEmpty()) {
            fillMap();
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        detailsTitle.setText(title);
    }

    /**
     * Bind the details view with {@link Poi} object.
     *
     * @param object Object with data
     */
    private void bindDetailsView(Poi object) {
        detailsBinding.setPoi(object);
    }

    /**
     * Fill the map with data
     */
    private void fillMap() {
        if (mapLoadedCallback == null) {
            final LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            // Add a marker in Sydney and move the camera
            for (Poi object : objectsMap.values()) {
                LatLng latLng = new LatLng(object.getLocation().getLatitude(), object.getLocation().getLongitude());
                boundsBuilder.include(latLng);
                map.addMarker(new MarkerOptions().position(latLng).title(object.getId()).icon(BitmapDescriptorFactory.defaultMarker(DEFAULT_MARKER_HUE)));
            }

            mapLoadedCallback = new MapLoadedCallback(boundsBuilder.build());
            map.setOnMapLoadedCallback(mapLoadedCallback);
        } else {
            markerCLickListener.selectedMarker = null;
            map.clear();
            boolean selectedFound = false;
            for (Poi object : objectsMap.values()) {
                LatLng latLng = new LatLng(object.getLocation().getLatitude(), object.getLocation().getLongitude());
                boolean isSelected = markerCLickListener.selectedId != null && object.getId().equals(markerCLickListener.selectedId);
                BitmapDescriptor icon = isSelected
                        ? BitmapDescriptorFactory.defaultMarker(SELECTED_MARKER_HUE)
                        : BitmapDescriptorFactory.defaultMarker(DEFAULT_MARKER_HUE);
                Marker marker = map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(object.getId())
                        .icon(icon));
                if (isSelected) {
                    selectedFound = true;
                    markerCLickListener.selectedMarker = marker;
                }
            }

            if (!selectedFound) {
                drawerBehaviour.hide(detailsView);
            }
        }
    }

    /**
     * Show error toast.
     *
     * @param message Error message
     */
    private void showError(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, getResources().getDimensionPixelSize(R.dimen.toast_offset_top));
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Load data.
     *
     * @param location Search area center location
     */
    private void loadData(final Location location) {
        if (isDataLoading) {
            return;
        }
        isDataLoading = true;
        Observable
                .fromCallable(new Callable<PoiResponse<List<Poi>>>() {
                    @Override
                    public PoiResponse<List<Poi>> call() throws Exception {
                        return PoiServiceProvider.getInstance().search(new PoiService.SearchRequest().center(location).limit(20));
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<PoiResponse<List<Poi>>>() {
                    @Override
                    public void call(PoiResponse<List<Poi>> response) {
                        if (response.getError() != null) {
                            PoiError.ResolveCallback callback = DataDispatcher.getInstance().getResolveCallback(response.getError(), MapsActivity.this);
                            response.getError().resolve(callback);
                        } else {
                            List<Poi> objects = response.getData();
                            objectsMap.clear();
                            for (Poi poi : objects) {
                                objectsMap.put(poi.getId(), poi);
                            }
                            if (!objectsMap.isEmpty() && map != null) {
                                fillMap();
                            }
                        }
                        isDataLoading = false;
                    }
                });
    }

    /**
     * Make reverse geo-coding search.
     *
     * @param search Search string
     */
    private void makeGeoSearch(final String search) {
        if (Geocoder.isPresent()) {
            Log.i(LOG_TAG, "makeGeoSearch called");
            final Geocoder geocoder = new Geocoder(this);
            searchPlaceProgress.setVisibility(View.VISIBLE);
            Observable.fromCallable(new Callable<List<Address>>() {
                @Override
                public List<Address> call() throws Exception {
                    try {
                        return geocoder.getFromLocationName(search, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<List<Address>>() {
                        @Override
                        public void call(List<Address> addresses) {
                            if (addresses != null && addresses.size() > 0) {
                                mapLoadedCallback = null;

                                Address address = addresses.get(0);
                                Location location = new Location("");
                                location.setLatitude(address.getLatitude());
                                location.setLongitude(address.getLongitude());
                                loadData(location);
                            } else {
                                showError("Could not find this location");
                            }
                            searchPlaceProgress.setVisibility(View.GONE);
                        }
                    });

        }
    }

    /**
     * Get default location.
     *
     * @return {@link Location} somewhere
     */
    private Location getDefaultLocation() {
        Location location = new Location("");
        location.setLatitude(52.370216);
        location.setLongitude(4.895168);
        return location;
    }

    private class MarkerCLickListener implements GoogleMap.OnMarkerClickListener {
        private String selectedId;
        private Marker selectedMarker;

        /**
         * (non-Javadoc)
         * @see com.google.android.gms.maps.GoogleMap.OnMarkerClickListener#onMarkerClick(Marker)
         */
        @Override
        public boolean onMarkerClick(Marker marker) {
            String id = marker.getTitle();
            Poi poi = objectsMap.get(id);
            bindDetailsView(poi);
            setTitle(poi.getTitle());
            if (drawerBehaviour.isHidden()) {
                drawerBehaviour.collapse(detailsView);
            }

            List<PoiImage> images = poi.getImages();
            if (images.size() > 0) {
                Glide.with(MapsActivity.this).load(images.get(0).getPath()).into(imageView);
            } else {
                imageView.setImageBitmap(null);
            }

            if (selectedId == null || !marker.getTitle().equals(selectedId)) {
                if (selectedMarker != null) {
                    selectedMarker.setIcon(BitmapDescriptorFactory.defaultMarker(DEFAULT_MARKER_HUE));
                }
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(SELECTED_MARKER_HUE));

                selectedId = id;
                selectedMarker = marker;
            }

            return true;
        }
    }

    private class MapLoadedCallback implements GoogleMap.OnMapLoadedCallback {
        private LatLngBounds bounds;

        public MapLoadedCallback(LatLngBounds bounds) {
            this.bounds = bounds;
        }

        @Override
        public void onMapLoaded() {
            if (bounds != null) {
                cameraChangeListener.setInternal(true);
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
            }
        }
    };

    private class CameraChangeListener implements GoogleMap.OnCameraChangeListener {

        private boolean isInternal;

        public void setInternal(boolean isInternal) {
            this.isInternal = isInternal;
        }

        @Override
        public void onCameraChange(CameraPosition cameraPosition) {
            if (!isInternal) {
                Location location = new Location("");
                location.setLatitude(cameraPosition.target.latitude);
                location.setLongitude(cameraPosition.target.longitude);
                loadData(location);
            }
            isInternal = false;
        }
    }
}
