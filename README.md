# README #

This is an example application illustrating map object details view similar to GoogleMaps on Android.

The app uses google maps, which require an API key. To be able to run it you'll need to obtain your own API key. Follow [this link](https://console.developers.google.com/flows/enableapi?apiid=maps_android_backend&keyType=CLIENT_SIDE_ANDROID) to get the key, then create a gradle.properties file in the project root and add your key as follows:
GoogleMapsApiKey=YourKeyHere.